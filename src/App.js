import React from 'react';
import { Provider } from 'react-redux';
import create from './stores';
import Masthead from './components/Masthead';
import { H1, HELP_TITLE, HELP_LIST } from './i18n/en';
import ImgList from './components/ImgList';
import Help from './components/Help';

const App = ({ data }) => (
  <Provider store={create({ data })}>
    <div className="App">
      <Masthead title={H1} />
      <main className="Main Masthead pa3 ph5-ns">
        <div className="mw9 center">
          <ImgList />
          <Help
            title={HELP_TITLE}
            list={HELP_LIST}
          />
        </div>
      </main>
    </div>
  </Provider>
);

export default App;
