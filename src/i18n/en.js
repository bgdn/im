export const H1 = 'Img mgmt';
export const DELETE_IMAGE_BUTTON = 'Delete';
export const DELETE_IMAGE_CONFIRMATION = 'Are you sure?';
export const HELP_TITLE = 'Click';
export const HELP_LIST = [
  'At row to see and edit full sized image',
  'Anywhere at an image to add a tooltip',
  'At tooltip to remove it',
  'At title or description to edit it',
];
export const CREATE_TOOLTIP_PROMPT = 'Tooltip content:';
