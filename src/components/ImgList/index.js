import React from 'react';
import { connect } from 'react-redux';
import pick from 'lodash/pick';
import Item from '../Item';
import './index.css';

const ImgList = ({ data }) => (
  <ul className="ImgList list pl0">
    {
      data ? data
        .filter(item => item.entities.image)
        .map((item, i) => {
          const { image, tooltips } = item.entities;
          const itemProps = pick(image[Object.keys(image)[0]], [
            'title',
            'imgPath',
            'desc',
            'id',
          ]);
          return (
            <Item
              key={i}
              tooltips={tooltips}
              {...itemProps}
            />
          );
        }) : null
    }
  </ul>
);

export default connect(state => state)(ImgList);
