/* global prompt */

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import pick from 'lodash/pick';
import startCase from 'lodash/startCase';
import * as actions from '../../actionCreators';
import Image from '../Image';

const Opened = (props) => {
  const { title, desc, close, id } = props;
  const imageProps = pick(props, [
    'id',
    'title',
    'imgPath',
    'tooltips',
  ]);
  const updateText = (type) => {
    const text = prompt();
    props[`update${startCase(type)}`]({
      id,
      [type]: text,
    });
  };

  return (
    <div className="expanded">
      <button
        className="close pa2 grow f3"
        onClick={close}
        children="&times;"
      />
      <Image {...imageProps} />
      <div className="pt2 pb3">
        <div
          className="title f3 mv3"
          onClick={updateText.bind(this, 'title')}
          children={title}
        />
        <div
          className="desc f5 measure-wide lh-copy"
          onClick={updateText.bind(this, 'desc')}
          children={desc}
        />
      </div>
    </div>
  );
};

export default connect(
  state => state,
  dispatch => bindActionCreators({
    updateTitle: actions.image.updateTitle,
    updateDesc: actions.image.updateDesc,
  }, dispatch))(Opened);
