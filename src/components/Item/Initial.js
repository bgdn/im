/* global confirm */

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actionCreators';
import { DELETE_IMAGE_BUTTON, DELETE_IMAGE_CONFIRMATION } from '../../i18n/en';

const Initial = ({ imgPath, title, deleteImage, id }) => {
  const del = (e) => {
    e.stopPropagation();
    if (confirm(DELETE_IMAGE_CONFIRMATION)) deleteImage(id);
  };

  return (
    <div className="initial">
      <div
        className="preview h3 w3 v-mid flex-none"
        style={{
          backgroundImage: `url(${imgPath})`,
        }}
        title={title}
      />
      <div className="title v-mid ma3">{title}</div>
      <button
        onClick={del}
        className="edit v-mid pa1 ma1 ba b--light-red br2 dim f6 light-red"
        children={DELETE_IMAGE_BUTTON}
      />
    </div>
  );
};

export default connect(
  state => state,
  dispatch => bindActionCreators({
    deleteImage: actions.image.delete,
  }, dispatch))(Initial);
