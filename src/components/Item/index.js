import React, { Component } from 'react';
import classnames from 'classnames';
import pick from 'lodash/pick';
import Initial from './Initial';
import Opened from './Opened';
import './index.css';

export default class Item extends Component {
  state = {
    opened: false,
  };

  componentWillMount() {
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  open() {
    this.setState({
      opened: true,
    });
  }

  close(e) {
    e.stopPropagation();
    this.setState({
      opened: false,
    });
  }

  render() {
    const { opened } = this.state;
    const initial = !opened;
    const props = pick(this.props, [
      'id',
      'title',
      'desc',
      'imgPath',
    ]);
    const init = <Initial {...props} />;
    const open = (
      <Opened
        {...props}
        close={this.close}
        tooltips={this.props.tooltips}
      />
    );
    return (
      <li
        className={classnames('Item bb b--black-10 pa3', {
          pr4: opened,
          opened,
        })}
        onClick={this.open}
        children={initial ? init : open}
      />
    );
  }
}
