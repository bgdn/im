import React from 'react';

const Masthead = ({ title }) => (
  <header className="Masthead pa3 ph5-ns bb b--black-20 bg-near-white">
    <div className="mw9 center">
      <h1 className="Title f5 ttu tracked ph3">{title}</h1>
    </div>
  </header>
);

export default Masthead;
