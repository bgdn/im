import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actionCreators';
import './index.css';

class Tooltip extends Component {
  componentWillMount() {
    this.deleteTooltip = this.deleteTooltip.bind(this);
  }

  deleteTooltip(e) {
    e.stopPropagation();
    const { imageId, config: { id } } = this.props;
    this.props.deleteTooltip({ imageId, id });
  }

  render() {
    const { config: { x, y, content } } = this.props;
    const style = {
      left: `${(x * 100)}%`,
      top: `${(y * 100)}%`,
    };

    return (
      <div
        className="Tooltip"
        style={style}
        onClick={this.deleteTooltip}
      >
        <div
          className="content bg-white shadow-1 pa2 br2 f6"
          onClick={e => { e.stopPropagation(); }}
          children={content}
        />
      </div>
    );
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({
    deleteTooltip: actions.tooltip.delete,
  }, dispatch))(Tooltip);
