import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actionCreators';
import Tooltip from '../Tooltip';
import { CREATE_TOOLTIP_PROMPT } from '../../i18n/en';
import './index.css';

class Image extends Component {
  componentWillMount() {
    this.createTooltip = this.createTooltip.bind(this);
  }

  createTooltip(e) {
    const { createTooltip, id } = this.props;
    const { left, top, width, height } = e.target.getBoundingClientRect();
    const content = prompt(CREATE_TOOLTIP_PROMPT, '');
    if (content && content.trim() !== '') {
      createTooltip({
        x: ((e.clientX - left) / width).toFixed(3) / 1,
        y: ((e.clientY - top) / height).toFixed(3) / 1,
        content,
        imageId: id,
      });
    }
  }

  render() {
    const {
      imgPath,
      title,
      tooltips,
      id,
    } = this.props;
    return (
      <div
        className="Image"
        onClick={this.createTooltip}
      >
        <img
          src={imgPath}
          alt={title}
        />
        {
          tooltips ? Object.keys(tooltips)
            .filter(key => tooltips[key])
            .map((key, i) => {
              return (
                <Tooltip
                  key={i}
                  imageId={id}
                  config={tooltips[key]}
                />
              );
            }) : null
        }
      </div>
    );
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({
    createTooltip: actions.tooltip.create,
  }, dispatch))(Image);
