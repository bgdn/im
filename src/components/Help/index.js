import React from 'react';

const Help = ({ title, list }) => (
  <div className="f6 pa3 w-50-l bg-washed-yellow">
    <p className="ma0 b">{title}</p>
    <ul className="list ma0 pa2 pv">
      {list.map((item, i) => <li className="pv1" key={i}>{item}</li>)}
    </ul>
  </div>
);

export default Help;
