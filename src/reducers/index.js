import deepAssign from 'deep-assign';
import uuid from 'uuid';
import {
  CREATE_TOOLTIP,
  DELETE_TOOLTIP,
  UPDATE_IMAGE_TITLE,
  UPDATE_IMAGE_DESC,
  DELETE_IMAGE
} from '../actions';

const getNewState = (state, obj, id) => {
  const newEntities = state.data.map((item) => {
    if (item.result === id) {
      return deepAssign(item, {
        entities: obj,
      });
    }
    return item;
  });
  return Object.assign({}, state, { data: newEntities });
};

const reducer = (state, action) => {
  switch (action.type) {
    case CREATE_TOOLTIP: {
      const { x, y, content, imageId } = action.payload;
      const id = uuid();
      return getNewState(state, {
        tooltips: {
          [id]: { x, y, content, id },
        },
      }, imageId);
    }

    case DELETE_TOOLTIP: {
      const { id, imageId } = action.payload;
      return getNewState(state, {
        tooltips: {
          [id]: false,
        },
      }, imageId);
    }

    case UPDATE_IMAGE_TITLE: {
      const { id, title } = action.payload;
      return getNewState(state, {
        image: {
          [id]: { title }
        },
      }, id);
    }

    case UPDATE_IMAGE_DESC: {
      const { id, desc } = action.payload;
      return getNewState(state, {
        image: {
          [id]: { desc }
        },
      }, id);
    }

    case DELETE_IMAGE: {
      const { id } = action.payload;
      return getNewState(state, {
        image: false,
      }, id);
    }

    default:
      return state;
  }
};

export default reducer;
