/* global window */

import { createStore, applyMiddleware, compose } from 'redux';
import createLogger from 'redux-logger';
import reducer from '../reducers';

const logger = createLogger();
const create = (state) => {
  return {
    ...createStore(
      reducer,
      state,
      compose(
        applyMiddleware(
          logger,
        ),
        window.devToolsExtension ? window.devToolsExtension() : $ => $,
      ),
    ),
  };
};

export default create;
