import {
  CREATE_IMAGE,
  UPDATE_IMAGE_TITLE,
  UPDATE_IMAGE_DESC,
  DELETE_IMAGE,
  CREATE_TOOLTIP,
  DELETE_TOOLTIP
} from '../actions';

export const image = {
  create(payload) {
    return {
      type: CREATE_IMAGE,
      payload,
    };
  },
  updateTitle(payload) {
    return {
      type: UPDATE_IMAGE_TITLE,
      payload,
    };
  },
  updateDesc(payload) {
    return {
      type: UPDATE_IMAGE_DESC,
      payload,
    };
  },
  delete(id) {
    return {
      type: DELETE_IMAGE,
      payload: { id },
    };
  },
};

export const tooltip = {
  create(payload) {
    return {
      type: CREATE_TOOLTIP,
      payload,
    };
  },
  delete(payload) {
    return {
      type: DELETE_TOOLTIP,
      payload,
    };
  },
};
