/* global document, fetch */

import React from 'react';
import ReactDOM from 'react-dom';
import { normalize, Schema, arrayOf } from 'normalizr';
import App from './App';
import './index.css';

const API_URI = 'stubbedResponse.json';
const imageSchema = new Schema('image', { idAttribute: 'id' });
const tooltipSchema = new Schema('tooltips', { idAttribute: 'id' });
imageSchema.define({
  tooltips: arrayOf(tooltipSchema),
});

(async() => {
  try {
    const response = await fetch(API_URI);
    const resJSON = await response.json();
    const data = resJSON.map(item => normalize(item, imageSchema));
    ReactDOM.render(
      <App data={data} />,
      document.getElementById('root')
    );
  } catch (e) {
    console.error('Something went wrong with the request', e);
  }
})();
