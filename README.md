# I M G M G M T

This sample project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## How to run

Run `npm start` to boot up Webpack server at [localhost:3000](http://localhost:3000).

You also may run `npm run build` to get minified and optimised project which is hosted at [im.surge.sh](https://im.surge.sh).

## How to use

**Just click**

* At row to see and edit full sized image
* Anywhere at an image to add a tooltip
* At tooltip to remove it
* At title or description to edit it

## Etc.

Here’s manually made JSON `stubbedResponse.json` in a `public` dir imitates server response with a pretty nested data. I haven’t used any tools for managing side-effects because the app is pretty straightforward.

I haven’t removed the `redux-logger`, so you can have a look in a browser console to know how the actions dispatch.
